resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

files {
	'custom/vehicles.meta',
	'custom/carvariations.meta',
	'custom/handling.meta',	
	'custom/carcols.meta'
}

data_file 'HANDLING_FILE' 'custom/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'custom/vehicles.meta'
data_file 'CARCOLS_FILE' 'custom/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'custom/carvariations.meta'

client_script 'vehicle_names.lua'

is_els 'true'

fx_version 'adamant'
games { 'gta5' }