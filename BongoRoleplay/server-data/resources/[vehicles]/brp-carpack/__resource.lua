resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

files {
    '16challenger/vehicles.meta',
    '16challenger/carvariations.meta',
    '16challenger/carcols.meta',
    '16challenger/handling.meta',
	
    'bugatti/vehicles.meta',
    'bugatti/carvariations.meta',
    'bugatti/carcols.meta',
    'bugatti/handling.meta',  
 
    'boss302/vehicles.meta',
    'boss302/carvariations.meta',
    'boss302/carcols.meta',
    'boss302/handling.meta', 	
    
	'fenyr/vehicles.meta',
    'fenyr/carvariations.meta',
    'fenyr/carcols.meta',
    'fenyr/handling.meta', 	
	
    'lwgtr/vehicles.meta',
    'lwgtr/carvariations.meta',
    'lwgtr/carcols.meta',
    'lwgtr/handling.meta', 
	
    'lykan/vehicles.meta',
    'lykan/carvariations.meta',
    'lykan/carcols.meta',
    'lykan/handling.meta', 	
	
    'mgt/vehicles.meta',
    'mgt/carvariations.meta',
    'mgt/carcols.meta',
    'mgt/handling.meta',   
	
	'p1/vehicles.meta',
    'p1/carvariations.meta',
    'p1/carcols.meta',
    'p1/handling.meta',	
	
	'rmodpagani/vehicles.meta',
    'rmodpagani/carvariations.meta',
    'rmodpagani/carcols.meta',
    'rmodpagani/handling.meta',
	
	'terzo/vehicles.meta',
    'terzo/carvariations.meta',
    'terzo/carcols.meta',
    'terzo/handling.meta',

	'zentenario/vehicles.meta',
    'zentenario/carvariations.meta',
    'zentenario/handling.meta',		
}

data_file 'VEHICLE_METADATA_FILE' '16challenger/vehicles.meta'
data_file 'CARCOLS_FILE' '16challenger/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' '16challenger/carvariations.meta'
data_file 'HANDLING_FILE' '16challenger/handling.meta'

data_file 'VEHICLE_METADATA_FILE' 'boss302/vehicles.meta'
data_file 'CARCOLS_FILE' 'boss302/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'boss302/carvariations.meta'
data_file 'HANDLING_FILE' 'boss302/handling.meta'

data_file 'VEHICLE_METADATA_FILE' 'bugatti/vehicles.meta'
data_file 'CARCOLS_FILE' 'bugatti/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'bugatti/carvariations.meta'
data_file 'HANDLING_FILE' 'bugatti/handling.meta'

data_file 'VEHICLE_METADATA_FILE' 'fenyr/vehicles.meta'
data_file 'CARCOLS_FILE' 'fenyr/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'fenyr/carvariations.meta'
data_file 'HANDLING_FILE' 'fenyr/handling.meta'

data_file 'VEHICLE_METADATA_FILE' 'lwgtr/vehicles.meta'
data_file 'CARCOLS_FILE' 'lwgtr/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'lwgtr/carvariations.meta'
data_file 'HANDLING_FILE' 'lwgtr/handling.meta'

data_file 'VEHICLE_METADATA_FILE' 'lykan/vehicles.meta'
data_file 'CARCOLS_FILE' 'lykan/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'lykan/carvariations.meta'
data_file 'HANDLING_FILE' 'lykan/handling.meta'

data_file 'VEHICLE_METADATA_FILE' 'mgt/vehicles.meta'
data_file 'CARCOLS_FILE' 'mgt/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'mgt/carvariations.meta'
data_file 'HANDLING_FILE' 'mgt/handling.meta'

data_file 'VEHICLE_METADATA_FILE' 'p1/vehicles.meta'
data_file 'CARCOLS_FILE' 'p1/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'p1/carvariations.meta'
data_file 'HANDLING_FILE' 'p1/handling.meta'

data_file 'VEHICLE_METADATA_FILE' 'rmodpagani/vehicles.meta'
data_file 'CARCOLS_FILE' 'rmodpagani/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'rmodpagani/carvariations.meta'
data_file 'HANDLING_FILE' 'rmodpagani/handling.meta'

data_file 'VEHICLE_METADATA_FILE' 'terzo/vehicles.meta'
data_file 'CARCOLS_FILE' 'terzo/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'terzo/carvariations.meta'
data_file 'HANDLING_FILE' 'terzo/handling.meta'

data_file 'VEHICLE_METADATA_FILE' 'zentenario/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'zentenario/carvariations.meta'
data_file 'HANDLING_FILE' 'zentenario/handling.meta'

