resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

files {	
    'dodgeEMS/handling.meta',
    'dodgeEMS/vehicles.meta',
    'dodgeEMS/carcols.meta',
    'dodgeEMS/carvariations.meta',
    'dodgeEMS/vehiclesettings.meta',
	---16fire
	'16fire/vehicles.meta',
    '16fire/carvariations.meta',
    '16fire/carcols.meta',		
    'rambulance/handling.meta',
    'rambulance/carcols.meta',
    'rambulance/carvariations.meta',
    'rambulance/vehicles.meta',
}

---ambo
data_file 'CARCOLS_FILE' 'rambulance/carcols.meta'
data_file 'HANDLING_FILE' 'rambulance/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'rambulance/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'rambulance/carvariations.meta'

data_file 'VEHICLE_METADATA_FILE' '16fire/vehicles.meta'
data_file 'CARCOLS_FILE' '16fire/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' '16fire/carvariations.meta'

---dodgeEMS
data_file 'HANDLING_FILE' 'dodgeEMS/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'dodgeEMS/vehicles.meta'
data_file 'CARCOLS_FILE' 'dodgeEMS/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'dodgeEMS/carvariations.meta'
data_file 'VEHICLE_SETTINGS_FILE' 'dodgeEMS/vehiclesettings.meta'

client_script 'vehicle_names.lua'


