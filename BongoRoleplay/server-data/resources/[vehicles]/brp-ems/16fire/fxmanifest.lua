resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

files {
    ---ambo 
	'ambo/vehicles.meta',
    'ambo/carvariations.meta',
    'ambo/carcols.meta',
    ---ambulance
	'ambulance/carcols.meta'	
	'ambulance/carvariations.meta',
	'ambulance/handling.meta',
	'ambulance/vehicles.meta',	
     ---ems cars	
	'dodgeEMS/carcols.meta'
	'dodgeEMS/carvariations.meta',
	'dodgeEMS/vehicles.meta',
	'dodgeEMS/handling.meta',	
	'dodgeEMS/vehiclesettings.meta',
	---ems cars	
	'carcols.meta'
	'carvariations.meta',
	'vehicles.meta',
	'handling.meta',	
	'vehiclesettings.meta',
	
}
---ambo
data_file 'VEHICLE_METADATA_FILE' 'ambo/vehicles.meta'
data_file 'CARCOLS_FILE' 'ambo/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'ambo/carvariations.meta'

---ambulance
data_file 'CARCOLS_FILE' 'ambulance/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'ambulance/carvariations.meta'
data_file 'HANDLING_FILE' 'ambulance/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'ambulance/vehicles.meta'

---ems cars
data_file 'HANDLING_FILE' 'dodgeEMS/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'dodgeEMS/vehicles.meta'
data_file 'CARCOLS_FILE' 'dodgeEMS/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'dodgeEMS/carvariations.meta'
data_file 'VEHICLE_SETTINGS_FILE' 'dodgeEMS/vehiclesettings.meta'

---ems cars
data_file 'HANDLING_FILE' 'handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'vehicles.meta'
data_file 'CARCOLS_FILE' 'carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'carvariations.meta'
data_file 'VEHICLE_SETTINGS_FILE' 'vehiclesettings.meta'


client_script 'vehicle_names.lua'

fx_version 'adamant'
games { 'gta5' }