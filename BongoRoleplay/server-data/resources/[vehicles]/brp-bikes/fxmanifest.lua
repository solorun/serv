fx_version 'adamant'
games { 'gta5' }

files {
  'batpod/handling.meta',
  'batpod/vehicles.meta',
  'batpod/carvariations.meta',   
  
  'hcbr17/handling.meta',
  'hcbr17/vehicles.meta',
  'hcbr17/carcols.meta',
  'hcbr17/carvariations.meta',   
  
  'hyabusadrag/handling.meta',
  'hyabusadrag/vehicles.meta',
  'hyabusadrag/carcols.meta',
  'hyabusadrag/carvariations.meta',   
  
  'jdbullet/handling.meta',
  'jdbullet/vehicles.meta',
  'jdbullet/carcols.meta',
  'jdbullet/carvariations.meta',  
  
  'r1/handling.meta',
  'r1/vehicles.meta',
  'r1/carcols.meta',
  'r1/carvariations.meta',
}

data_file 'HANDLING_FILE' 'batpod/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'batpod/vehicles.meta'
data_file 'VEHICLE_VARIATION_FILE' 'batpod/carvariations.meta'

data_file 'HANDLING_FILE' 'hcbr17/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'hcbr17/vehicles.meta'
data_file 'CARCOLS_FILE' 'hcbr17/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'hcbr17/carvariations.meta'

data_file 'HANDLING_FILE' 'hyabusadrag/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'hyabusadrag/vehicles.meta'
data_file 'CARCOLS_FILE' 'hyabusadrag/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'hyabusadrag/carvariations.meta'

data_file 'HANDLING_FILE' 'jdbullet/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'jdbullet/vehicles.meta'
data_file 'CARCOLS_FILE' 'jdbullet/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'jdbullet/carvariations.meta'

data_file 'HANDLING_FILE' 'r1/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'r1/vehicles.meta'
data_file 'CARCOLS_FILE' 'r1/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'r1/carvariations.meta'


client_script 'vehicle_names.lua'