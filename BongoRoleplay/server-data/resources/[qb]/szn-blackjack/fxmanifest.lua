fx_version 'adamant'
game "gta5"

name 'sezzin'
description 'Playable Blackjack at the casino, similar to GTAOnline.'
author 'sezzin - https://github.com/Xinerki/'
url 'https://github.com/Xinerki/kgv-blackjack'

shared_script 'coords.lua'
client_script 'timerbars.lua'
client_script 'client.lua'
server_script 'server.lua'

-- data_file 'DLC_ITYP_REQUEST' 'stream/tables/vw_prop_vw_tables.ityp'
-- data_file 'DLC_ITYP_REQUEST' 'stream/cards/vw_prop_vw_cards.ityp'
