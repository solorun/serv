QB = {}

QB.Spawns = {
    ["legion"] = {
        coords = {
            x = -206.39, 
            y = -1014.12, 
            z = 30.13, 
            h = 68.73
        },
        location = "legion",
        label = "Train Station",
    },

    ["policedp"] = {
        coords = {
            x = 433.74, 
            y = -644.59, 
            z = 28.78, 
            h = 89.5 
        },
        location = "policedp",
        label = "City Bustand",
    },

    ["paleto"] = {
        coords = {
            x = 80.35, 
            y = 6424.12, 
            z = 31.67, 
            h = 45.5 
        },
        location = "paleto",
        label = "Paleto Bay",
    },

    ["motel"] = {
        coords = {
            x = 327.56, 
            y = -205.08, 
            z = 53.08, 
            h = 163.5 
        }, 
        location = "motel",
        label = "Motels",
    },
}