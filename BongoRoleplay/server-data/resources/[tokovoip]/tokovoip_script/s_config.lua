TokoVoipConfig = {
	channels = {
		{name = "PD Radio", subscribers = {}},
		{name = "EMS Radio", subscribers = {}},
		{name = "PD/BCSO/EMS Shared Radio", subscribers = {}},
		{name = "BCSO Radio", subscribers = {}},
		{name = "PD/BCSO Shared Radio", subscribers = {}}
	}
};